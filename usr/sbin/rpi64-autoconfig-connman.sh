#!/bin/sh

set -e

CONNMAN_DIR=/var/lib/connman
CONNMAN_WIFI_CONFIG=$CONNMAN_DIR/wifi.config

# Credentials from the following file are used to setup the wifi connection on
# startup. Variables WIFI_SSID and WIFI_KEY must be defined in plain-text, e.g.:
#   WIFI_SSID=mywifissid
#   WIFI_KEY=mypassphrase
USER_WIFI_CONFIG=/boot/firmware/wifi.config

if [ ! -f $USER_WIFI_CONFIG ]; then
	echo "File $USER_WIFI_CONFIG not found"
	exit 0
fi

WIFI_SSID="$(sed -n 's/^WIFI_SSID=\(.*\)$/\1/p' -- $USER_WIFI_CONFIG)"
WIFI_KEY="$(sed -n 's/^WIFI_KEY=\(.*\)$/\1/p' -- $USER_WIFI_CONFIG)"

if [ -z "$WIFI_SSID" ] || [ -z "$WIFI_KEY" ]; then
	echo "Missing WiFi credentials in $USER_WIFI_CONFIG"
	exit 0
fi

mkdir -p $CONNMAN_DIR

cat << EOF > $CONNMAN_WIFI_CONFIG
[service_wifi]
Type=wifi
Name=$WIFI_SSID
Passphrase=$WIFI_KEY
EOF
